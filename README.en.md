<div align="right">
    <a href="https://gitee.com/lsl-gitee/LDevKit/blob/master/README.md">中文</a>
</div>

# LDevKit

## Project brief
>As a code farmer, what I hate most is to do some repetitive and tedious work. Therefore, I want to develop some 
>tools (plugin) to help myself avoid some repetitive and tedious operations and improve my development efficiency. 
>Here I also share some of my achievements with friends who have the same needs.

## IDE plugins

### [MybatisHelperPro](https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro)

> Visual mybatis decompiler helps you generate entity, Dao interface and mapper mapping files according to database tables.

## Java components
### [Annotation-based distributed locking（redis)](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-lock/README.md)
> Annotations based implementation of distributed lock Java components.

### [Public Enumeration Components](https://gitee.com/lsl-gitee/rdf-components/blob/optimize-basicsEnumSub/rdf-component-enum/README.md)
> Provides several commonly used enumeration methods, no need to copy and paste, while providing automatic enumeration description.

## Communication
>The current plug-ins are developed and maintained by individuals, find problems or optimization suggestions can be through QQ or message me,
> waiting for you at any time.

- The official QQ group：1083977145
- E-mail：lsl.yx@foxmail.com

![QQ Group Number：1083977145](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/qq_group.png "QQ Group Number：1083977145")

## Donate
> I become stronger, bald head, to a spicy dry tofu can help you maintain beautiful hair！
- [aliPay](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/alipay.jpg)
- [weiChatPay](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/weiChatPay.png)
