# 版本说明/Version Description

## 1.1.2
- 优化项：
	- 生成代码上的优化，优化生成类的注解使用更规范的格式，如下图所示：   
	![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/version/1.1.2/20220412222134.png "在这里输入图片标题")
	- `连接信息页`添加不同数据类型的驱动切换快捷按钮和连接方式的切换，可直接选择使用`URL`连接方式，直接在`URL`输入框输入需要连接的URL地址，也可以选择`Automatic`连接方式，通过数据框输入对应的连接信息，如下图所示:
	![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/version/1.1.2/20220412221559.png "在这里输入图片标题")
	- 原生成代码退格用的是tab，现在优化成4个空格替换一个tab，更符合idea的代码规范；
	- 修复`Sqlite`驱动，选择多级目录时，连接URL错误问题；
	- 代码层面对主要生成代码逻辑进行了重构优化；
	- 去除原过期的刷新项目目录方式`BaseDir.refresh()`;

## 1.1.1
- 优化项：
	- 生成配置页优化，会记住上次设置的内容，包括`生成路径/Path`、`包名/Package`和其他勾选注解等，省去了你每次生成都要重新设置的麻烦；
	- 设置页可以同时打开多个不同设置页，该版本将这个问题优化成只能同时打开一个设置页，打开新的设置页是，旧设置页会被关闭；
	- 其他设置页保存成功时，自动关闭页面，减少再次点击关闭的麻烦；
- bug修复：
	- 修复驱动设置保存时，其他设置会被重置默认配置问题；
	- 选择表页异步获取库表数据刷新页面时报错问题；
	- 修复生成代码时未命中`其他设置/Other settings`->`忽略前缀/Ignore prefx`时，生成文件会自带默认文件后缀问题；

## 1.1.0 → [Issue](https://gitee.com/lsl-gitee/LDevKit/issues/I2X37P)

- The extension supports multiple data sources, including: `MySQL、MySQL5、Oracle、PostgreSQL、SQLite、MariaDB、Microsoft SQL Server`;
- Added `数据类型/Data type` menu to support custom `JdbcType` to `JavaType` data type conversion;
- Added `其他设置/Other settings` menu, etc;
- 扩展可支持多数据源，包括：`MySQL、MySQL5、Oracle、PostgreSQL、SQLite、MariaDB、Microsoft SQL Server`；
- 新增`数据类型/Data type`菜单，支持自定义数据类型转换`JdbcType`到`JavaType`；
- 新增`其他设置/Other settings`菜单，等；

## 1.0.6 → [Issue](https://gitee.com/lsl-gitee/LDevKit/issues/I34WY8)

- Code generation prompts optimization.
- 代码生成提示优化

## 1.0.5 → [Issue](https://gitee.com/lsl-gitee/LDevKit/issues/I2VR17)

- Entity, Dao interfaces, and Mapper mapping files are also optionally generated. Unneeded files are not generated;
- Since they are now microservices, the generated files may be under different modules, so the generation path is optimized to be optional.
- Entity、Dao接口和Mapper映射文件也可选生成，不需要的可不生成;
- 由于现在都是微服务，生成的文件可能在不同module下，因此生成路径也会优化成可选；

## 1.0.4 → [Issue](https://gitee.com/lsl-gitee/LDevKit/issues/I2Y7OE)

- After the file generation is completed, take the initiative to refresh the project directory and display the generated file.
- 文件生成完成后主动刷新项目目录，显示生成文件

## 1.0.3

- Fixed field duplicate generation bug.
- 修复字段重复生成bug

## 1.0.2-release

- Fixed database field uppercase conversion bug.
- 修复数据库字段大写转换bug

## 1.0.1-release

- Updated version compatibility to 182+.
- 更新版本兼容性至182+

## 1.0.0-release

- It supports inputting database information, logging in database, selecting single or multiple database tables, configuring generation code rules, executing generation entities, Dao interface and mapper.
- 支持输入数据库信息登录数据库，选择单个或多个库表，配置生成代码规则，并执行生成实体，Dao接口与Mapper。