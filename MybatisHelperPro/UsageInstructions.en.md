<div align="right">
    <a href="https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/UsageInstructions.md">中文</a>
</div>


# Usage Instructions

---

### 1. Plugin locations
After the installation of the plugin, you can find the plugin `LDevKit-> MybatisHelperPro` in the top menu, and click to 
open the plugin, as shown in the figure: 
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123113343.png "在这里输入图片标题")

### 2. `连接信息/Connection info` window
This window is used to input the database information that needs to be connected, including `Host address, Port, Database,
 Username and Password` and other information, the URL is displayed directly below. The `测试连接/TestConn` button can test 
 whether the connection can be successful. Click `确定/Ok` to jump to the next page. The default data source is `MySQL5.1`,
 It can be found in `设置/Settings -> 驱动/Driver` custom data source types and connection drivers.

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220110040.png "在这里输入图片标题")

### 3. `选择表/Selection table` window
This window is used to select the table that needs to generate the code. Hold down `Ctrl` key to make multiple radio choices, 
press `Shift` key to make multiple choices, `换库/SwitchDb` button to return to the `连接信息/Connection info` window,
reenter the database information that needs to be connected, and click`下一步/Next` to jump to the next page.

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220162551.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220162625.png "在这里输入图片标题")

### 4. `生成配置/Generated config` window
The window mainly contains two parts, the left menu is the table you select in the `选择表/Selection table` window, the `重选/Reselect` 
button in the lower left can jump back to the previous page to re-select the table, select the table and click `Remove`
to remove it from the generated list; Entity, DAO interface, Mapper mapping file `module` can be selected separately, 
at the same time can choose whether to generate this part of the code, the following is for `Entity class, DAO interface,
 Mapper mapping file` generated separate configuration;

- i. For Entity class configuration, you can choose whether to add database annotation and serialization. 
It supports normal `Getter and Setter` mode, and also supports `Lombok` annotation mode,
The location of file generation is determined by `Module + build path + package name`;
   
- ii. For `Dao interface` configuration, support adding `@Repository` or `@Mapper` annotation, File generation location 
rules are the same as above;

- iii. For the `Mapper mapping file` configuration, support generation of `insert, insertSelection, update, updateSelection, 
queryOne, delete` methods, File generation location rules are the same as above，Because the `queryOne、delete` methods 
operate on primary keys, they will not be generated if the table does not have a primary key; 

Click the `生成/Generate` button to generate the file at the appropriate location.  

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220163157.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220163230.png "在这里输入图片标题")


### 5. Generate code
Here are some of the code generation effects.

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123114356.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123120736.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123120619.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123120826.png "在这里输入图片标题")

### 6. Generate configuration notes
just look at this drawing！

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220105214.png "在这里输入图片标题")

### 7. Driver Settings

Driver Settings Settings include：`模式/Pattern、驱动类型/Driver Type、驱动名称/Driver Name、URL模板/URL Template、驱动路径/Driver Path`；

- **模式/Pattern：** selectable`自带驱动/Own driver` or `外部驱动/External driver`；

- **驱动类型/Driver Type：** when 模式/Pattern selected `外部驱动/External driver` can select item include：`MySQL for 5.1、MySQL、Oracle、PostgreSQL、
SQLite、MariaDB、Microsoft SQL Server`;

- **驱动名称/Driver Name：** disable，Shows the different types of driver names；

- **URL模板/URL Template：** selectable，Each driver type has at least one URL template to choose from, and custom edits can be made 
while the template is selected，for example, the URL template is appended to the end of the parameter，However, it is recommended not 
to modify the parameter `${xx}`，Otherwise, the connection cannot match the correct parameters, resulting in a connection failure!

- **驱动路径/Driver Path：** when 模式/Pattern selected `外部驱动/External driver` enable，Optional custom connection drivers；

In order to keep the plug-in as light as possible, there is only MySQL5.1 for the built-in driver at present, 
so you must configure the connection driver path when choosing the external driver, or you will not find the connection driver!

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220164900.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220164916.png "在这里输入图片标题")

### 8. Data type settings
数据类型/Data type Settings are customizable `JdbcType` to `JavaType` conversion, the Settings panel contains `Original conversion table`
and `Custom conversion table`, `Custom conversion table` it's going to be overwritten by `JdbcType` （**The value of the custom conversion 
table JdbcType must be exactly the same as the value of the original conversion table to be overridden**）;

As for `Package`, only packages that require an explicit import need to write the package path, for example `java.util.Date` need an explicit import，
so need in `Custom conversion table` write it, `java.lang.String` no explicit import is required, so you don't have to write `Package`;

As following picture shows: The data type of the database field is `TIMESTAMP`, the generated Java data type will be `Date` import package
will be `java.util.Date`, it's replaces the original table conversion relationship;

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220173508.png "在这里输入图片标题")

**Note: As shown above, if the database field contains `TIMESTAMP` and `DATE` data type, at this point, you will find that 
Java needs to import packages `java.util.Date` and `java.sql.Date`, this is not allowed in Java, at this point we can also 
custom override the `DATE` type, the import package is set to `java.util.Date`.**

### 9. Other Settings

其他设置/Other settings the settable contents include:

- **作者/Author：** the author name of the annotation generated when configuring the generated code. Default value `MybatisHelperPro`； 

- **忽略前缀/Ignore prefix：** for example, our table is `smbms_user`，the class that we normally generate is called `SmbmsUser`，now if you set the 
value here to be `smbms`（case Insensitive），the generated class name will become `User`，`smbms` would be omitted. No default value；

- **实体类后缀/Entity suffix：** for example, our table is `user`，the class that we normally generate is called `User`，now if you set the 
value here to be `Do`，the generated class name will become `UserDo`. Default value`Entity`；

- **Dao类后缀/Dao suffix：** for example, our table is `user`，the class that we normally generate is called `User`，now if you set the 
value here to be `Dao`，the generated class name will become `UserDao`. Default value`Mapper`；

- **Mapper文件后缀/Mapper suffix：** for example, our table is `user`，the class that we normally generate is called `User`，now if you set the 
value here to be `Repository`，the generated class name will become `UserRepository`. Default value`Mapper`；

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220180652.png "在这里输入图片标题")