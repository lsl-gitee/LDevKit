<div align="right">
    <a href="https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/README.en.md">English</a>
</div>
<div align="center">
    <img src="https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/icon.png" width="180" alt="icon"/>
</div>

<div align="center">
    <strong font-size="35px">MybatisHelperPro</strong>
</div>

---

## 背景
由于`Mybatis`官方的反编译工具感觉很繁琐，不好用，于是我就自己写一个满足自己需求的插件，现在我想分享出来帮助更多有需要的人。

## 特点
- 可视化的数据库Java代码生成工具，可快速生成Java的`实体类`，`Dao接口`和`Mapper.xml`映射文件；

- 支持多种数据源，`1.1.x`版本开始支持多种数据源，包括：`MySQL、MySQL5、Oracle、PostgreSQL、SQLite、MariaDB、Microsoft SQL Server`，同时可自定义连接驱动；

- 自定数据类型转换，`JdbcType`到`JavaType`可以自定义转换，比如`varchar`类型你可以选择转成`String`类型，也可以选择转成`Object`类型，只要你想都可以；

- 支持自定义生成文件后缀，生成忽略前缀，支持生成`Lombok`注解、实现序列化接口和添加数据库注解等；

- 可支持IDE产品包括：`IntelliJ IDEA Ultimate、IntelliJ IDEA Community、IntelliJ IDEA Educational、MPS、AppCode、PhpStorm、 
PyCharm Professional、PyCharm Community、PyCharm Educational、Rider、RubyMine、WebStorm、Android Studio、CLion、DataGrip、GoLand`；

## 产品展示
![生成主页](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220103246.png "生成主页")
  
## 安装方式
### 方式一
IDE菜单选择`File`->`Settings`->`Plugins`->`Marketplace`搜索框中输入`MybatisHelperPro`,就能看到我们的插件了，然后点击右上角的`install`按钮就能完成安装了。

![安装方式一](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210129222040.png "安装方式一")
### 方式二
从我们的[**历史版本**](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version)中下载安装包，然后选择IDE菜单`File`->`Settings`->`Plugins`,
点击设置按钮，选择`install Plugin from Disktop`选项，然后选择从我们历史版本中下载的zip安装包进行安装，建议安装最新版本。

![安装方式二](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210129222129.png "安装方式二")

## 使用说明
- [中文](https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/UsageInstructions.md)
- [English](https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/README.en.md)

## 交流
>当前插件由个人进行开发和维护，发现问题或有优化建议可通官方QQ群或Issue留言告知本人，插件的更新动态会第一时间在QQ群里通知，同时也能得到本人的即时响应，欢迎大家进群讨论。

- 官方QQ群：1083977145
- 官方邮箱：lsl.yx@foxmail.com

![QQ群号：1083977145](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/qq_group.png "QQ群号：1083977145")

## 投喂
> 码字不易，工作之余码字更是全凭毅力!
- [支付宝投喂](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/alipay.jpg)
- [微信投喂](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/weiChatPay.png)

## 历史版本
- [1.1.2](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#112)
![new](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/icon/new%20(1).png "new")
- [1.1.1](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#111)
- [1.1.0](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#110--issue)
- [1.0.6](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#106--issue) 
- [1.0.5](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#105--issue) 
- [1.0.4](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#104--issue) 
- [1.0.3](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#103)
- [1.0.2-release](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#102) 
- [1.0.1-release](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#101)
- [1.0.0-release](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#100)

## 更多
- [插件首页](https://plugins.jetbrains.com/plugin/15913-mybatishelperpro)
- [Issue](https://gitee.com/lsl-gitee/LDevKit/issues)