<div align="right">
    <a href="https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/UsageInstructions.en.md">English</a>
</div>

# 使用说明

---

### 1. 插件位置
插件安装完成后，可在顶部菜单找到插件 `LDevKit -> MybatisHelperPro`，点击打开插件，如图：
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123113343.png "在这里输入图片标题")

### 2. `连接信息/Connection info`窗口
该窗口用于输入需要连接的数据库信息，包括 `主机地址Host、端口Port、数据库Database、用户名User和密码Password`等信息，URL将直接展示在下方，
`测试连接/TestConn`按钮可以测试是否能连接成功，点击 `确定/Ok` 可以跳转至下一个页面，默认数据源为`MySQL5.1`，可在`设置/Settings -> 驱动/Driver`
自定义数据源类型和连接驱动。

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220110040.png "在这里输入图片标题")

### 3. `选择表/Selection table`窗口
该窗口用于选择需要生成代码的表，按住`ctrl`键可进行多个单选，按`shift`键可进行多选，`换库/SwitchDb`按钮可返回`连接信息/Connection info`窗口，
重新输入需要连接的数据库的信息，点击`下一步/Next`可跳转至下一页。

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220162551.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220162625.png "在这里输入图片标题")

### 4. `生成配置/Generated config` 窗口
该窗口主要包含两大部分，左边菜单是你在`选择表/Selection table`窗口选择的表，左下边`重选/Reselect`按钮可跳回上一个页面重新选择表，
选中表点击`移除/Remove`可从生成列表中移除； Entity、Dao接口、Mapper映射文件`module`可单独选择，同时可以选择是否生成这部分代码，
下边是针对`实体类、dao接口、Mapper映射文件`生成的单独配置；

- i、针对`Entity类`的配置，可选择是否添加数据库注解和序列化，支持普通的`Getter and Setter`方式，也支持`lombok`注解方式，
文件生成位置会根据 `Module+生成路径+包名`决定；

- ii、针对`Dao接口`的配置，支持添加`@Repository`或`@Mapper`注解，文件生成位置规则同上；

- iii、针对`Mapper映射文件`的配置，支持生成`insert、insertSelection、update、updateSelection、queryOne、delete`方法，文件生成位置规则同上，
因为`queryOne、delete`方法是依据主键操作的，如果表没有主键，这两个方法将无法生成；

点击`生成/Generate`按钮即可在对于的位置生成文件。 

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220163157.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220163230.png "在这里输入图片标题")


### 5. 生成代码
下面是部分代码生成效果。

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123114356.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123120736.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123120619.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210123120826.png "在这里输入图片标题")

### 6. 生成配置说明
看图吧！

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220105214.png "在这里输入图片标题")

### 7. 驱动设置

驱动设置/Driver settings设置内容包括：`模式/Pattern、驱动类型/Driver Type、驱动名称/Driver Name、URL模板/URL Template、驱动路径/Driver Path`；

- **模式/Pattern：** 可选`自带驱动/Own driver`或`外部驱动/External driver`；

- **驱动类型/Driver Type：** 当模式/Pattern选择`外部驱动/External driver`时可选，可选项有：`MySQL for 5.1、MySQL、Oracle、PostgreSQL、
SQLite、MariaDB、Microsoft SQL Server`;

- **驱动名称/Driver Name：** 不可选，展示不同类型的驱动名称；

- **URL模板/URL Template：** 可选，每个驱动类型都有至少一个URL模板可选，在选定模板的同时可进行自定义编辑，如在URL模板最后追加参数，
但`${xx}`这类参数建议不要修改，否则连接时无法匹配正确的参数，导致连接失败！

- **驱动路径/Driver Path：** 当模式/Pattern选择`外部驱动/External driver`时可选，可选择自定义的连接驱动；

为了尽可能保持插件轻量，目前自带驱动只有MySQL5.1，所以选择外部驱动时必须要配置连接驱动路径，否则将找不到连接驱动！
 
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220164900.png "在这里输入图片标题")
![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220164916.png "在这里输入图片标题")

### 8. 数据类型设置
数据类型/Data type 设置可自定义`JdbcType`和`JavaType`的转换，该设置面板包含`原始的转换关系表`和`自定义转换关系表`，`自定义转换关系表`
会根据`JdbcType`进行覆盖（**自定义转换表JdbcType的值要跟原始转换表的值一模一样才能进行覆盖**）;

关于`Package`，只有需要显式import的包才需要写包路径，比如`java.util.Date`需要显式import，所以需要在`自定义转换关系表`写明，`java.lang.String`
不需要显式import，所以不用写`Package`；

如下图：数据库字段数据类型为`TIMESTAMP`，生成Java数据类型将为`Date`导入的包`java.util.Date`，替换了原始表的转换关系;

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220173508.png "在这里输入图片标题")

**注：如上图，如果数据库字段包含`TIMESTAMP`和`DATE`数据类型，这时你会发现Java需要导入的包有`java.util.Date`和`java.sql.Date`，
这样的写法在Java中是不被允许的，这时我们可以将`DATE`类型也进行自定义覆盖，导入的包设置成`java.util.Date`就可以了。**

### 9. 其他设置

其他设置/Other settings 可设置内容包括：

- **作者/Author：** 配置生成代码时生成注释的作者名，默认值`MybatisHelperPro`； 

- **忽略前缀/Ignore prefix：** 如我们的表为`smbms_user`，正常情况我们生成的类名为`SmbmsUser`，此时如果你这里设置的值为`smbms`（不区分大小写），
则生成的类名会变成`User`，`smbms`将被忽略，无默认值；

- **实体类后缀/Entity suffix：** 如我们的表为`user`，正常情况我们生成的类名为`User`，此时如果你这里设置的值为`Do`，则生成的类名会变成`UserDo`，默认值`Entity`；

- **Dao类后缀/Dao suffix：** 如我们的表为`user`，正常情况我们生成的类名为`User`，此时如果你这里设置的值为`Dao`，则生成的类名会变成`UserDao`，默认值`Mapper`；

- **Mapper文件后缀/Mapper suffix：** 如我们的表为`user`，正常情况我们生成的类名为`User`，此时如果你这里设置的值为`Repository`，
则生成的类名会变成`UserRepository`，默认值`Mapper`；

![输入图片说明](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220180652.png "在这里输入图片标题")
