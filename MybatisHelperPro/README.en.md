<p align="right">
    <a href="https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/README.md">中文</a>
</p>
<p align="center">
    <img src="https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/icon.png" width="180" alt="icon"/>
</p>

<p align="center">
    <strong style="font-size: 50px;">MybatisHelperPro</strong>
</p>

---

## Background
Because the official decompiler tool of `Mybatis` is not easy to use, I feel very cumbersome, so I want to write a 
plug-in to meet my own needs, now I want to share it to help more people in need.

## Features
- Visual database Java code generation tool, can quickly generate java `Entity class`, `DAO interface` and `Mapper.xml` mapping file;

- Multiple data sources are supported, version`1.1.x`starts to support multiple data sources, including: `MySQL、MySQL5、
Oracle、PostgreSQL、SQLite、MariaDB、Microsoft SQL Server`, you can also customize the connection driver;

- Custom data type conversion, `JdbcType` to `JavaType` you can customize the conversion, for example, for `varchar` 
you can choose to turn it into `String` or `Object`, if you want;

- Support custom build file suffix, generate ignored prefix, support to generate `Lombok` annotation, 
serialization interface and add database annotation, etc;

- Available IDE products include: `IntelliJ IDEA Ultimate、IntelliJ IDEA Community、IntelliJ IDEA Educational、MPS、AppCode、
PhpStorm、PyCharm Professional、PyCharm Community、PyCharm Educational、Rider、RubyMine、WebStorm、Android Studio、CLion、DataGrip、GoLand`

## Product show
![Main page](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210220103246.png "Main page")

## Installation
### Mode one
From the IDE menu, select `File`->`Settings`->`Plugins`->`Marketplace` and type `MybatisHelperPro` into the search box. Then click the `install` button in the upper right corner to install the plugin.

![Mode one](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210129222040.png "Mode one")
### Mode two

From our [**History version**](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version) download the installation package, and then select the IDE menu `File`->`Settings`->`Plugins`,
Click the Settings button, select the `install Plugin from Disktop` option, and then select the ZIP installation package downloaded from our history version to install. It is recommended to install the latest version.

![Mode two](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/20210129222129.png "Mode two")

## Usage Instructions
- [中文](https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/UsageInstructions.md)
- [English](https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro/README.en.md)

## Communication
>At present, plug-ins are developed and maintained by individuals. If you find any problems or have suggestions for optimization,
> you can inform me through the official QQ group or Issue message. The update of plug-ins will be notified in the QQ group
> in the first time, and you can also get an immediate response from me.

- The official QQ group：1083977145
- The official E-mail：lsl.yx@foxmail.com

![QQ Group Number：1083977145](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/qq_group.png "QQ Group Number：1083977145")

## Donate
> Code word is not easy, after work code word is all by perseverance!
- [aliPay](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/alipay.jpg)
- [weiChatPay](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/weiChatPay.png)

## History version
- [1.1.2](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#112)
![new](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/icon/new%20(1).png "new")
- [1.1.1](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#111)
- [1.1.0](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#110--issue)
- [1.0.6](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#106--issue) 
- [1.0.5](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#105--issue) 
- [1.0.4](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#104--issue) 
- [1.0.3](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#103)
- [1.0.2](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#102) 
- [1.0.1](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#101)
- [1.0.0](https://gitee.com/lsl-gitee/LDevKit/tree/master/MybatisHelperPro/version/README.md#100)

## More
- [Plugin Home](https://plugins.jetbrains.com/plugin/15913-mybatishelperpro)
- [Issue](https://gitee.com/lsl-gitee/LDevKit/issues)