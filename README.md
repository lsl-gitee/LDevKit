<div align="right">
    <a href="https://gitee.com/lsl-gitee/LDevKit/blob/master/README.en.md">English</a>
</div>

# LDevKit

## 项目简介
>作为一名码农，最讨厌做一些重复而繁琐的工作，为此我想开发一些工具（插件）帮助自己避免一些重复而繁琐的工作，以提高自己的开发效率，
>这里我也将我的一些成果分享出来，帮助有同样需要的朋友。

## IDE插件

### [MybatisHelperPro](https://gitee.com/lsl-gitee/LDevKit/blob/master/MybatisHelperPro)
> 可视化的Mybatis反编译工具，帮助你根据数据库表生成entity，dao接口和Mapper映射文件。

## Java组件
### [基于注解方式的分布式锁（redis)](https://gitee.com/lsl-gitee/rdf-components/blob/master/rdf-component-lock/README.md)
> 基于注解实现的分布式锁Java组件

### [公共枚举组件](https://gitee.com/lsl-gitee/rdf-components/blob/optimize-basicsEnumSub/rdf-component-enum/README.md)
> 提供枚举常用的几种方法，再也不用复制粘贴了，同时提供枚举描述的自动透出。


## 交流
>当前插件由个人进行开发和维护，发现问题或有优化建议可通官方QQ群或Issue留言告知本人，插件的更新动态会第一时间在QQ群里通知，同时也能得到本人的即时响应，欢迎大家进群讨论。

- 官方QQ群：1083977145
- 官方邮箱：lsl.yx@foxmail.com

![QQ群号：1083977145](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/qq_group.png "QQ群号：1083977145")

## 投喂
> 码字不易，工作之余码字更是全凭毅力!
- [支付宝投喂](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/alipay.jpg)
- [微信投喂](https://gitee.com/lsl-gitee/LDevKit/raw/master/MybatisHelperPro/img/weiChatPay.png)